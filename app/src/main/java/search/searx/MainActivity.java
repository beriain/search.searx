package search.searx;

import android.app.Activity;
import android.os.Bundle;
import android.os.Build;
import android.view.WindowManager;
import android.webkit.WebView;
import android.content.SharedPreferences;
import android.widget.EditText;
import android.widget.TextView;
import android.view.inputmethod.EditorInfo;
import android.view.KeyEvent;
import android.view.View;
import android.content.Intent;
import android.view.inputmethod.InputMethodManager;
import android.app.AlertDialog;
import android.text.method.LinkMovementMethod;
import android.content.DialogInterface;
import android.preference.PreferenceManager;
import android.webkit.WebViewClient;
import android.view.LayoutInflater;
import android.net.Uri;
import android.webkit.URLUtil;
import android.widget.Toast;
import android.webkit.JavascriptInterface;

public class MainActivity extends Activity {

    public String instance = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        setContentView(R.layout.activity_main);

        //Check if an instance has been set
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        instance = preferences.getString("pref_instance", "");
        //if there is no instance selected open list to select one
        if (instance.compareTo("") == 0) {
            WebView wv = (WebView) findViewById(R.id.viewer);
            wv.requestFocus();
            selectInstance(new View(MainActivity.this));
        }
        

        EditText et = (EditText) findViewById(R.id.searchfield);
        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search(view.getText().toString());
                    view.setText("");
                    View v = MainActivity.this.getCurrentFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(MainActivity.this.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        WebView wv = (WebView) findViewById(R.id.viewer);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.addJavascriptInterface(new jsinterface(), "Android");
        wv.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //let opening the preferences inside the app
                if (url.startsWith(instance))
                    return false;
                else { //otherwise launch a web browser
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                    return true;
                }
            }
        });
    }

    public void search(String searchTerm) {
        if (instance.compareTo("") == 0) {
            selectInstance(new View(MainActivity.this));
        } else {
            WebView wv = (WebView) findViewById(R.id.viewer);
            wv.getSettings().setJavaScriptEnabled(true);
            wv.loadUrl(instance + "?q=" + searchTerm);

            /*POST - not working
            wv.postUrl(instance, Base64.encode(searchTerm.getBytes(), Base64.DEFAULT));*/
        }
    }

    public void selectInstance(View view) {
        WebView wv = (WebView) findViewById(R.id.viewer);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("file:///android_asset/instanceParser.html");
        //hide keyboard
        View v = MainActivity.this.getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(MainActivity.this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    public class jsinterface {
        @JavascriptInterface
        public void setInstance(String i) {
            instance = i;
            PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit().putString("pref_instance", instance).commit();
            Toast.makeText(MainActivity.this, instance + " " + getResources().getString(R.string.selected), Toast.LENGTH_SHORT).show();
            final WebView wv = (WebView) findViewById(R.id.viewer);
            wv.post(new Runnable() {
                @Override
                public void run() {
                    wv.getSettings().setJavaScriptEnabled(true);
                    wv.loadUrl(instance);
                }
            });
        }
    }

    public void displayAbout(View view) {
        Intent i = new Intent(MainActivity.this, AboutActivity.class);
        MainActivity.this.startActivity(i);
    }
}
