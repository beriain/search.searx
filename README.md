# <img src="https://framagit.org/uploads/-/system/project/avatar/70271/ic_launcher.png?width=22" alt="" height="40"> Search searx

An unofficial Searx app.

With a homescreen widget and an easy way to select instances.

This app can be built using [Debian Android SDK](https://bits.debian.org/2017/03/build-android-apps-with-debian.html)